import { Meteor } from 'meteor/meteor';
import { Features } from '../imports/api/features.js';
import '../imports/api/files.js';
import '../imports/api/features.js';

import '../imports/routes/route-main.js';
import '../imports/routes/route-home.js';
import '../imports/routes/route-editor.js';

Meteor.startup(() => {
	var features = Features.find().count();

	function populateFeatures() {
		Features.insert({
			title: 'Easy to use',
			content: 'This is a very basic, supear easy to use text editor. Just click browse or just paste some text, do your edits and then save.',
			createdAt: new Date()

		});

		Features.insert({
			title: 'Hackable',
			content: 'The text editor can be easily integrated into other apps. New features can be added to fit your needs.',
			createdAt: new Date()
		});

		Features.insert({
			title: 'Meteor',
			content: 'Web Edit is written with the help of Meteor JS full-stack framework. Meteor allows for easy integration with thousand of packages such as bootstrap, angularJS, reactJS.',
			createdAt: new Date()
		});
	}
/* TODO Add active class functionality to nav
import { Active } from '../imports/api/active.js';
import '../imports/api/active.js';

	var active = Active.find().count();
	function populateActive() {
		Active.insert({
			home: true,
			editor: false
		});
	}
	if(!active) {
		populateActive();
	} else {
		console.log('features present');
	}
*/
	if(!features) {
		populateFeatures();
	} else {
		console.log('features present');
	}


});
