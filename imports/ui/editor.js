import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Files } from '../api/files.js';

import './editor.html'

Template.editor.onCreated(function editorOnCreated() {
  Meteor.subscribe('files');

});

var height =  $(window).height();

Template.editor.helpers({
	rows() {
	// Editor height
    	return height/30;
	}
});

// events
Template.editor.events({
	'change .fileLoader'(event) {
        // Get the file
        const file = event.target.files[0];

        // Create new filereader object
        const fileReader = new FileReader();

        // On load copy the file content into textarea
        fileReader.onload = function(e) {
          const text = e.target.result;
          $('.Text').val(text);
          $('#textarea1').trigger('autoresize');
        };


        // Use the readAsText method of the fileReader object
        // to read the file -> triggers onload event
        fileReader.readAsText(file, 'UTF-8');

	},
	'click #save'() {
		// Get textarea content
		var content = $('.Text').val();         

		// Create new blob from textarea content
		var textBlob = new Blob([content], {type: 'text/plain'});      

		// Get the file name form     
		var title = $('.Title').val();

		// Create a new link (anchor) element in the DOM
		var link = document.createElement('a');                             

		// Set link download property to val of title
		link.download = title;

		// Set link content (set text node)
		link.innerHTML = 'Download File';

		if(window.webkitURL != null) {
		// If browser is chrome
			// Create href property for the created link
			link.href = window.URL.createObjectURL(textBlob);
		} else {     
			// Create href property for the created link
			link.href = window.URL.createObjectURL(textBlob);

			// On click remove the anchor element from DOM
			link.onclick = function(e) {
				document.body.removeChild(e.target);
			};

			// Hide the link
			link.style.display = 'none';
			
			// Append the element to the body
			document.body.appendChild(link);
		}

		// Trigger click event on the recently created link
		link.click();

		// preventDefault()
		return false;
	},
	'click #add'() {
		var title = $('.Title').val();
		var content = $('.Text').val();

		Meteor.call('files.insert', title, content);

		return false;
	}
});
