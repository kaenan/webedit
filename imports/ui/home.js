import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Features } from '../api/features.js';

// >> Imported templates
import './feature.js';
import './editor.js';

// >> Body HTML
import './home.html';

Template.home.onCreated(function homeOnCreated() {
  Meteor.subscribe('features');

});

// Reusables
var height =  $(window).height();
/*

  'click a'() {
    $('html, body').animate({
      scrollTop: $('.features').offset().top
    }, 'slow');
    return false;
  },

 */

// helpers
Template.home.helpers({
  features() {
    return Features.find({});
  },
  headerHeight() {
  // Height of top-content
    return height+'px';
  },
});

// events
Template.home.events({

});
