import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Features = new Mongo.Collection('features');

if (Meteor.isServer) {
	// This code only runs on the server
	Meteor.publish('features', function featuresPublication() {
		return Features.find({});
	});
}