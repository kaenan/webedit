import { Template } from 'meteor/templating';
import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Files = new Mongo.Collection('files');

if (Meteor.isServer) {
	// This code only runs on the server
	Meteor.publish('files', function filesPublication() {
		return Files.find({});
	});
}

Meteor.methods({
	'files.insert'(title, content) {
		check(title, String);

		// Make sure the user is logged in before inserting a task
		/*if (! Meteor.userId()) {
		  throw new Meteor.Error('not-authorized');
		}*/

		Files.insert({
		  title,
		  content,
		  createdAt: new Date(),
		});
	},
});