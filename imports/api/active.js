import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

export const Active = new Mongo.Collection('active');

if (Meteor.isServer) {
	// This code only runs on the server
	Meteor.publish('active', function activePublication() {
		return Active.find({});
	});
}